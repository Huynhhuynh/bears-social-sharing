var webpack = require('webpack');
var path = require('path');
var MiniCssExtractPlugin = require("mini-css-extract-plugin");

var config = {
    devtool: 'source-map',
    entry: {
        backend: ['./src/js/bss-backend.js', './src/scss/bss-backend.scss'],
        frontend: ['./src/js/bss-frontend.js', './src/scss/bss-frontend.scss'],
    },
    output: {
        path: path.resolve( __dirname, 'dist' ),
        filename: 'js/[name].bsocialsharing.bundle.js',
    },
    mode: 'development',
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    { loader: 'css-loader', options: { url: false, sourceMap: true } }
                ]
            },
            {
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    { loader: 'css-loader', options: { url: false, sourceMap: true } },
                    { loader: 'sass-loader', options: { sourceMap: true } }
                ]
            }
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: "./css/[name].bsocialsharing.css",
            chunkFilename: "./css/[id].bsocialsharing.css",
        })
    ],
}

module.exports = config;