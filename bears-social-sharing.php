<?php
/*
 * Plugin Name: Bears Social Sharing
 * Plugin URI: http://bearsthemes/
 * Description: Social Network Sharing Plugin - Created by Bearsthemes
 * Version: 1.0.1
 * Author: Bearsthemes
 * Author URI: http://bearsthemes.com/
 * Text Domain: bears-social-sharing
 */

/**
 * Composer autoload
 */
require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/inc/helpers.php';
require __DIR__ . '/inc/hooks.php';

if(! function_exists('bss_plugin_crb_load')) {
    /**
     * @since 1.0.1 
     */
    function bss_plugin_crb_load() {
        \Carbon_Fields\Carbon_Fields::boot();
    }
    add_action( 'after_setup_theme', 'bss_plugin_crb_load' );
}


if( ! function_exists('bss_plugin_links') ) {
    /**
     * Plugin page links
     * @since 1.0.0
     */
    function bss_plugin_links( $links ) {

        $plugin_links = array(
            '<a href="mailto:bearsthemes@gmail.com">' . __( 'Support', 'bears-social-sharing' ) . '</a>',
            '<a href="#">' . __( 'Docs', 'bears-social-sharing' ) . '</a>',
        );

        return array_merge( $plugin_links, $links );
    }

    add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), 'bss_plugin_links' );
}

if( ! class_exists('Bears_SocialSharing') ) {

    /**
     * Bears Megamenu - Main class
     * @since 1.0.0
     */
    class Bears_SocialSharing {

        /**
         * @since 1.0.0
         */
        function __construct() {
            

            $this->defined();
            $this->hooks();
        }

        /**
         * @since 1.0.0
         */
        public function defined() {
            define( "BSS_DIR_URL"    , plugin_dir_url( __FILE__ ) );
            define( "BSS_DIR_PATH"   , plugin_dir_path( __FILE__ ) );
            define( "BSS_VERSION"    , '1.0.0' );
        }

        /**
         * @since 1.0.0
         */
        public function hooks() {
            // load script
            add_action( 'wp_enqueue_scripts', array($this, 'scripts') );
            add_action( 'admin_enqueue_scripts', array($this, 'admin_scripts') );
        }

        public function admin_scripts() {
            wp_enqueue_script( 'b-social-sharing-back', BSS_DIR_URL . '/dist/js/backend.bsocialsharing.bundle.js', array('jquery'), BSS_VERSION, true );
            wp_enqueue_style( 'b-social-sharing-back', BSS_DIR_URL . '/dist/css/backend.bsocialsharing.css', array(), BSS_VERSION );
        }

        /**
         * scripts.
         * @since 1.0.0
         */
        public function scripts() {
            wp_enqueue_script( 'b-social-sharing-front', BSS_DIR_URL . '/dist/js/frontend.bsocialsharing.bundle.js', array('jquery'), BSS_VERSION, true );
            wp_enqueue_style( 'b-social-sharing-front', BSS_DIR_URL . '/dist/css/frontend.bsocialsharing.css', BSS_VERSION );
            wp_localize_script( 'b-social-sharing-front', 'bss_object', apply_filters( 'bss_object_localize_script', array(
                'ajax_url' => admin_url( 'admin-ajax.php' ),
            ) ) );
        }

    }

    $GLOBALS['Bears_SocialSharing'] = new Bears_SocialSharing();
}
