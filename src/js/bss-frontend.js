/**
 * @package Bears Social Sharing - Front
 * @author Bearsthemes
 */

// import bss_jssocials from 'jssocials'; 
import '../vendor/jssocials/jssocials';

!(function(w, $) {
    'use strict';
    var $BSS_Social_Element = $("#bsocial-sharing-element-container");
    var $BSS_Social_Container = $BSS_Social_Element.find('.bsocial-sharing__inner');
    
    // Dom Ready
    $(function() {
        new window.jsSocials.Socials( $BSS_Social_Container, bss_object.bss_settings );
        $BSS_Social_Element.on('click', '.__bss-toggle-social-sharing', function(e) {
            e.preventDefault();
            $BSS_Social_Container.toggleClass('__is-toogle-true');
        })
    })

    $(window).load(function() {
        $BSS_Social_Container.addClass('_active-animate');
    })
})(window, jQuery) 