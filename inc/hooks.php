<?php 

add_action( 'carbon_fields_register_fields', 'BSS_Setings' );
add_filter( 'bss_object_localize_script', 'BSS_Push_Options' );

add_action( 'wp_footer', 'BSS_Add_Social_Element' );