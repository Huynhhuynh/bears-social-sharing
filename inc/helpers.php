<?php

if( ! function_exists('BSS_Setings') ) {
    /**
     * @since 1.0.0
     * Register social sharing options
     * 
     * @return void
     */
    function BSS_Setings() {
        Carbon_Fields\Container::make( 'theme_options', __( 'Social Sharing', 'bears-social-sharing' ) )
            ->set_page_parent( 'options-general.php' )
            ->add_tab( __('General Settings', 'bears-social-sharing'), array(
                Carbon_Fields\Field::make( 'select', 'bss_post_types', __('Post Types', 'bears-social-sharing') )
                    ->add_options( array(
                        'posts_only' => __('Posts only', 'bears-social-sharing'),
                        'custom' => __('Custom', 'bears-social-sharing'),
                        'disable' => __('Disable', 'bears-social-sharing'),
                    ) )
                    ->set_help_text( 'Enable Social Sharing for pages, posts and custom post types. Note: Enable default post only.', 'bears-social-sharing' )
                    ->set_width( 50 ),

                Carbon_Fields\Field::make( "multiselect", "bss_custom_post_types", __('Custom Post Types', 'bears-social-sharing') )
                    ->set_conditional_logic( array(
                        array(
                            'field' => 'bss_post_types',
                            'value' => 'custom',
                        )
                    ) )
                    ->add_options( 'BSS_Custom_Post_Types_Opts_Func' )
                    ->set_default_value( 'post' )
                    ->set_width( 50 ),
                
                Carbon_Fields\Field::make( "multiselect", "bss_enable_social_share", __('Social Share With', 'bears-social-sharing') )
                    ->add_options( 'BSS_Social_Share_Support_Opts_Func' )
                    ->set_default_value( array('facebook', 'twitter', 'pinterest') ),
            ) )

            ->add_tab( __('Label & Icon Shares Options', 'bears-social-sharing'), array(
                Carbon_Fields\Field::make( 'html', 'label_icon_shares_tab_des' )
                    ->set_html( '<p>The build-in social network shares have following configuration</p>' ),
                
                Carbon_Fields\Field::make( 'separator', 'bss_separator_style_options_0', 'E-mail' ),

                Carbon_Fields\Field::make( 'text', 'bss_email_label', __('E-mail label', 'bears-social-sharing') )
                    ->set_default_value('E-mail')
                    ->set_width( 50 ),
                Carbon_Fields\Field::make( 'text', 'bss_email_icon', __('E-mail icon', 'bears-social-sharing') )
                    ->set_default_value('fa fa-at')
                    ->set_width( 50 ),

                Carbon_Fields\Field::make( 'separator', 'bss_separator_style_options_1', 'Twitter' ),

                Carbon_Fields\Field::make( 'text', 'bss_twitter_label', __('Twitter label', 'bears-social-sharing') )
                    ->set_default_value('Tweet')
                    ->set_width( 50 ),
                Carbon_Fields\Field::make( 'text', 'bss_twitter_icon', __('Twitter icon', 'bears-social-sharing') )
                    ->set_default_value('fa fa-twitter')
                    ->set_width( 50 ),
                Carbon_Fields\Field::make( 'text', 'bss_twitter_via', __('Twitter via', 'bears-social-sharing') )
                    ->set_default_value('')
                    ->set_width( 50 ),
                Carbon_Fields\Field::make( 'text', 'bss_twitter_hashtags', __('Twitter hashtags', 'bears-social-sharing') )
                    ->set_default_value('')
                    ->set_width( 50 ),

                Carbon_Fields\Field::make( 'separator', 'bss_separator_style_options_2', 'Facebook' ),

                Carbon_Fields\Field::make( 'text', 'bss_facebook_label', __('Facebook label', 'bears-social-sharing') )
                    ->set_default_value('Like')
                    ->set_width( 50 ),
                Carbon_Fields\Field::make( 'text', 'bss_facebook_icon', __('Facebook icon', 'bears-social-sharing') )
                    ->set_default_value('fa fa-facebook')
                    ->set_width( 50 ),

                Carbon_Fields\Field::make( 'separator', 'bss_separator_style_options_3', 'Google Plus' ),

                Carbon_Fields\Field::make( 'text', 'bss_googleplus_label', __('Google Plus label', 'bears-social-sharing') )
                    ->set_default_value('+1')
                    ->set_width( 50 ),
                Carbon_Fields\Field::make( 'text', 'bss_googleplus_icon', __('Google Plus icon', 'bears-social-sharing') )
                    ->set_default_value('fa fa-google-plus')
                    ->set_width( 50 ),

                Carbon_Fields\Field::make( 'separator', 'bss_separator_style_options_4', 'Linkedin' ),

                Carbon_Fields\Field::make( 'text', 'bss_linkedin_label', __('Linkedin label', 'bears-social-sharing') )
                    ->set_default_value('Share')
                    ->set_width( 50 ),
                Carbon_Fields\Field::make( 'text', 'bss_linkedin_icon', __('Linkedin icon', 'bears-social-sharing') )
                    ->set_default_value('fa fa-linkedin')
                    ->set_width( 50 ),

                Carbon_Fields\Field::make( 'separator', 'bss_separator_style_options_5', 'Pinterest' ),

                Carbon_Fields\Field::make( 'text', 'bss_pinterest_label', __('Pinterest label', 'bears-social-sharing') )
                    ->set_default_value('Pin it')
                    ->set_width( 50 ),
                Carbon_Fields\Field::make( 'text', 'bss_pinterest_icon', __('Pinterest icon', 'bears-social-sharing') )
                    ->set_default_value('fa fa-pinterest')
                    ->set_width( 50 ),
            ) )

            ->add_tab( __('Design Options', 'bears-social-sharing'), array(
                Carbon_Fields\Field::make( 'select', 'bss_show_label', __('Show Label', 'bears-social-sharing') )
                    ->add_options(array(
                        'yes' => __('Yes', 'bears-social-sharing'),
                        'no' => __('No', 'bears-social-sharing'),
                    ))
                    ->set_default_value('no'),

                Carbon_Fields\Field::make( 'select', 'bss_show_count', __('Show Count', 'bears-social-sharing') )
                    ->add_options(array(
                        'yes' => __('Yes', 'bears-social-sharing'),
                        'no' => __('No', 'bears-social-sharing'),
                        'inside' => __('Inside', 'bears-social-sharing'),
                    ))
                    ->set_default_value('no'),

                Carbon_Fields\Field::make( 'select', 'bss_share_in', __('Share In', 'bears-social-sharing') )
                    ->add_options(array(
                        'blank' => __('Blank', 'bears-social-sharing'),
                        'popup' => __('Popup', 'bears-social-sharing'),
                        'self' => __('Self', 'bears-social-sharing'),
                    ))
                    ->set_default_value('popup'),
                
                Carbon_Fields\Field::make( 'select', 'bss_position', __('Display Position', 'bears-social-sharing') )
                    ->add_options(array(
                        'sticky-on-top-left' => __('Sticky on the Top — Left', 'bears-social-sharing'),
                        'sticky-on-center-left' => __('Sticky on the Center — Left', 'bears-social-sharing'),
                        'sticky-on-bottom-left' => __('Sticky on the Bottom — Left', 'bears-social-sharing'),
                    ))
                    ->set_default_value('sticky-on-bottom-left'),
            ) );
    }
}

if(! function_exists('BSS_Custom_Post_Types_Opts_Func')) {
    /**
     * @since 1.0.0 
     */
    function BSS_Custom_Post_Types_Opts_Func() {
        $result = array(
            'post' => __('Post', 'bears-social-sharing'),
            // 'page' => __('Page', 'bears-social-sharing'),
        );

        $args = array(
            'public'   => true,
            '_builtin' => false
        );
        $post_types = get_post_types( $args, 'objects' );

        if( $post_types && count($post_types) > 0 ) {
            foreach( $post_types as $_pt ) {
                $result[$_pt->name] = $_pt->label;
            }
        }

        return apply_filters( 'bbs_custom_post_types_opts_filter', $result );
    }
}

if(! function_exists('BSS_Social_Share_Support_Opts_Func')) {
    /**
     * @since 1.0.0 
     */
    function BSS_Social_Share_Support_Opts_Func() {
        return apply_filters( 'bbs_social_share_support_opts_filter', array(
            'email' => __('E-mail', 'bears-social-sharing'),
            'twitter' => __('Twitter', 'bears-social-sharing'),
            'facebook' => __('Facebook', 'bears-social-sharing'),
            'googleplus' => __('Google Plus', 'bears-social-sharing'),
            'linkedin' => __('Linkedin', 'bears-social-sharing'),
            'pinterest' => __('Pinterest', 'bears-social-sharing'),
            // 'stumbleupon' => __('StumbleUpon', 'bears-social-sharing'),
            // 'whatsapp' => __('WhatsApp', 'bears-social-sharing'),
        ) );
    }
}

if(! function_exists('BSS_Add_Social_Element')) {
    /**
     * @since 1.0.0 
     */
    function BSS_Add_Social_Element() {
        global $post;
        $post_type = get_post_type($post);
        $is_single = is_single();
        $bss_post_types = carbon_get_theme_option( 'bss_post_types' );
        $bss_custom_post_types = carbon_get_theme_option( 'bss_custom_post_types' );

        if( empty($bss_post_types) || $bss_post_types == 'disable' ) {
            return;
        } else if( $bss_post_types == 'custom' ) {
            // check post type && single page
            if( ! in_array($post_type, $bss_custom_post_types) || ! $is_single ) return;
        } else {
            // posts_only
            if( ! in_array($post_type, array('post')) || ! $is_single ) return;
        }
    
        $bss_position = carbon_get_theme_option( 'bss_position' );
        ?>
        <div id="bsocial-sharing-element-container" class="bsocial-position-<?php echo esc_attr($bss_position); ?>">
            <?php do_action('bss_social_sharing_before'); ?>
            <div class="bsocial-sharing__inner bsocial-sharing-has-animate bsocial-sharing-ui-flat"></div>
            <?php do_action('bss_social_sharing_after'); ?>
            <div class="__bss-toggle-social-sharing">
                <?php _e('Sharing', 'bears-social-sharing'); ?>
                <span class="fa fa-angle-down"></span>
            </div>
        </div>
        <?php
    }
}

if(! function_exists('BSS_Push_Options')) {
    function BSS_Push_Options($data) {
        global $post;

        $shares = carbon_get_theme_option( 'bss_enable_social_share' );
        $showLabel = carbon_get_theme_option( 'bss_show_label' );
        $showCount = carbon_get_theme_option( 'bss_show_count' );
        $shareIn = carbon_get_theme_option( 'bss_share_in' );
        $mapReplace = array(
            'yes' => true,
            'no' => false,
            'inside' => 'inside',
        );

        $share_data = array();
        if($shares && count($shares) > 0) {
            foreach($shares as $s) {
                $item_share_opts = array(
                    'share' => $s,
                    'label' => carbon_get_theme_option( 'bss_'. $s .'_label' ),
                    'logo' => carbon_get_theme_option( 'bss_'. $s .'_icon' ),
                );

                array_push( $share_data, $item_share_opts );
            }
        }

        $data['bss_settings'] = array(
            // 'url' => 'https://dribbble.com/', // get_permalink( $post ),
            'text' => get_the_title( $post ),
            'showLabel' => $mapReplace[$showLabel],
            'showCount' => $mapReplace[$showCount],
            'shareIn' => $shareIn,
            'shares' => $share_data,
        );

        return $data;
    }
}